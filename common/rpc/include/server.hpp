#ifndef ABEILLE_RPC_SERVER_H_
#define ABEILLE_RPC_SERVER_H_

#include <grpcpp/grpcpp.h>

#include <atomic>
#include <condition_variable>
#include <memory>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

#include "common/errors/include/errors.hpp"

namespace abeille {
namespace rpc {

struct ServiceInfo {
  std::string address;
  grpc::Service *service;
};

class Server {
 public:
  Server() = default;
  explicit Server(std::vector<ServiceInfo> &services) noexcept
      : services_(services) {}

  Server(Server &&other) noexcept;
  Server &operator=(Server &&other) noexcept;

  error Run();
  void Shutdown();

 private:
  void init();
  void launch_and_wait();
  inline bool is_ready() const noexcept {
    return ready_;
  }

 private:
  std::vector<ServiceInfo> services_;

  std::thread thread_;
  grpc::ServerBuilder builder_;
  std::unique_ptr<grpc::Server> server_ = nullptr;

  std::mutex mutex_;
  bool ready_ = false;
  std::condition_variable cv_;

  std::atomic<bool> shutdown_ = false;
};

}  // namespace rpc
}  // namespace abeille

#endif  // ABEILLE_RPC_SERVER_H_
