#ifndef ABEILLE_UTILS_CONVERT_HPP_
#define ABEILLE_UTILS_CONVERT_HPP_

#include <cstdint>
#include <iterator>
#include <string>

#include "common/errors/include/errors.hpp"
#include "common/rpc/proto/abeille.grpc.pb.h"

uint64_t address2uint(const std::string &addrZZess);

std::string uint2address(uint64_t n);

// ExtractAddress extracts address from the strings of the form
// "protocol:ip:port".
std::string ExtractAddress(const std::string &str);

// Split splits the string by the given character.
// It ignores repeated delimiter characters.
std::vector<std::string> Split(const std::string &str, char c);

template <typename RepeatedField>
auto Repeated2Vec(RepeatedField rf) {
  return std::vector(rf.begin(), rf.end());
}

error GetFileData(const std::string &filepath, std::string &data) noexcept;

#endif  // ABEILLE_UTILS_CONVERT_HPP_
