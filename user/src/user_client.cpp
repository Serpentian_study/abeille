#include "user/include/user_client.hpp"

#include <chrono>
#include <vector>

#include "common/utils/include/convert.hpp"
#include "common/utils/include/logger.hpp"
#include "user/include/registry.hpp"

using grpc::ClientContext;
using grpc::Status;

namespace abeille {
namespace user {

Client::Client(std::shared_ptr<Registry> registry) noexcept
    : registry_(registry) {}

void Client::CommandHandler(const ConnResp &resp) {
  error err;
  switch (resp.command()) {
    case USER_COMMAND_REDIRECT:
      err = handleCommandRedirect(resp);
      break;
    case USER_COMMAND_ASSIGN:
      err = handleCommandAssign(resp);
      break;
    case USER_COMMAND_RESULT:
      err = handleCommandResult(resp);
      break;
    default:
      break;
  }

  if (!err.ok()) {
    LOG_ERROR(err);
  }
}

error Client::UploadData(const std::string &filename, const Bytes &task_data) {
  status_ = USER_STATUS_UPLOAD_DATA;
  registry_->task_descriptions.push({filename, task_data});

  std::unique_lock<std::mutex> lk(mutex_);
  cv_.wait(lk, [this] { return status_ == USER_STATUS_IDLE; });

  return error();
}

error Client::handleCommandRedirect(const ConnResp &resp) {
  leader_id_ = resp.leader_id();
  reconnect(uint2address(resp.leader_id()));
  return error();
}

error Client::handleCommandAssign(const ConnResp &resp) {
  registry_->task_statuses.insert_or_assign(
      resp.task_state().task_id().filename(), Registry::TaskStatus::PENDING);
  return error();
}

error Client::handleCommandResult(const ConnResp &resp) {
  if (resp.task_state().task_result().empty()) {
    return error("empty result");
  }

  std::string filename = resp.task_state().task_id().filename();
  LOG_INFO("received result for [%s]", filename.c_str());

  error err =
      registry_->SaveTaskResult(filename, resp.task_state().task_result());
  if (!err.ok()) {
    return err;
  }

  registry_->task_statuses.insert_or_assign(
      resp.task_state().task_id().filename(), Registry::TaskStatus::COMPLETED);

  return error();
}

void Client::StatusHandler(ConnReq &req) {
  {
    std::lock_guard<std::mutex> lk(mutex_);

    error err;
    switch (status_) {
      case USER_STATUS_UPLOAD_DATA:
        err = handleStatusUploadData(req);
        break;
      default:
        break;
    }

    if (err.ok()) {
      req.set_status(status_);
    } else {
      req.set_status(USER_STATUS_IDLE);
    }

    status_ = USER_STATUS_IDLE;
  }

  cv_.notify_one();
}

error Client::handleStatusUploadData(ConnReq &req) {
  if (registry_->task_descriptions.empty()) {
    return error("empty task descriptions queue");
  }

  auto &task_description = registry_->task_descriptions.front();
  req.set_lib(registry_->shared_task_library);
  req.set_filename(task_description.filename);
  req.set_task_data(task_description.task_data);
  registry_->task_descriptions.pop();

  return error();
}

}  // namespace user
}  // namespace abeille
