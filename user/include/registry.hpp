#ifndef ABEILLE_USER_REGISTRY_HPP_
#define ABEILLE_USER_REGISTRY_HPP_

#include <filesystem>
#include <queue>
#include <string>
#include <unordered_map>

#include "common/errors/include/errors.hpp"
#include "common/utils/include/types.hpp"
#include "user/proto/task.pb.h"

namespace abeille {
namespace user {

struct Registry {
  struct TaskDescription {
    std::string filename;
    Bytes task_data;
  };

  enum TaskStatus { PENDING, COMPLETED };
  static constexpr const char *TaskStatusName[] = {"pending", "completed"};

  error Init(const std::string &filepath) noexcept;

  void SetTaskResultsDirectory(const std::string &dirpath) noexcept;

  error LoadSharedTaskLibrary(const std::string &filepath) noexcept;
  error SaveTaskResult(const std::string &filename, const Bytes &task_result) const noexcept;

  std::string shared_task_library;
  std::filesystem::path task_results_dir;
  std::queue<TaskDescription> task_descriptions;
  std::unordered_map<std::string, TaskStatus> task_statuses;
};

}  // namespace user
}  // namespace abeille

#endif  // ABEILLE_USER_REGISTRY_HPP_
