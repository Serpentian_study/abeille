#include "worker/include/worker_client.hpp"

#include <fstream>

#include "common/utils/include/convert.hpp"
#include "common/utils/include/logger.hpp"
#include "worker/proto/ipc.pb.h"

using grpc::ClientContext;
using grpc::Status;

namespace abeille {
namespace worker {

Client::Client(std::shared_ptr<Registry> registry,
               std::shared_ptr<IPC> ipc) noexcept
    : registry_(registry), ipc_(ipc) {}

void Client::CommandHandler(const ConnResp &resp) {
  error err;
  switch (resp.command()) {
    case WORKER_COMMAND_REDIRECT:
      err = handleCommandRedirect(resp);
      break;
    case WORKER_COMMAND_ASSIGN:
      err = handleCommandAssign(resp);
      break;
    case WORKER_COMMAND_PROCESS:
      err = handleCommandProcess(resp);
      break;
    default:
      break;
  }

  if (!err.ok()) {
    LOG_ERROR(err);
  }
}

error Client::handleCommandAssign(const ConnResp &resp) {
  if (resp.task_id().client_id() == 0) {
    return error("client id is zero");
  }

  status_ = WORKER_STATUS_BUSY;
  LOG_INFO("got assigned [%s] task", resp.task_id().filename().c_str());

  return error();
}

error Client::handleCommandProcess(const ConnResp &resp) {
  if (resp.task_data().empty()) {
    return error("empty task data");
  }

  error err = registry_->SaveLibrary(resp.task_id().client_id(), resp.lib());
  if (!err.ok()) {
    return err;
  }

  // TODO: make something more elegant and async
  processTaskData(resp.task_id(), resp.task_data());

  return error();
}

error Client::handleCommandRedirect(const ConnResp &resp) {
  leader_id_ = resp.leader_id();
  reconnect(uint2address(resp.leader_id()));
  return error();
}

void Client::StatusHandler(ConnReq &req) {
  if (!task_states_.empty()) {
    status_ = WORKER_STATUS_COMPLETED;
  }

  error err;
  switch (status_) {
    case WORKER_STATUS_COMPLETED:
      err = handleStatusCompleted(req);
      break;
    default:
      break;
  }

  if (err.ok()) {
    req.set_status(status_);
  } else {
    req.set_status(WORKER_STATUS_IDLE);
  }

  status_ = WORKER_STATUS_IDLE;
}

error Client::handleStatusCompleted(ConnReq &req) {
  auto &task_state = task_states_.front();
  LOG_DEBUG("completed [%s] from [%s]", task_state.task_id().filename().c_str(),
            uint2address(task_state.task_id().client_id()).c_str());
  req.set_allocated_task_state(new TaskState(std::move(task_state)));
  task_states_.pop();
  return error();
}

void Client::processTaskData(const TaskID &task_id, const Bytes &task_data) {
  IPCRequest req;
  req.set_user_id(task_id.client_id());
  req.set_data(task_data);

  Bytes task_result;
  LOG_INFO("process task data via IPC...");
  error err = ipc_->ProcessTask(req, task_result);
  if (!err.ok()) {
    LOG_ERROR(err);
  }

  TaskState task_state;
  task_state.set_allocated_task_id(new TaskID(task_id));
  task_state.set_task_result(task_result);
  task_states_.push(task_state);
  LOG_INFO("finished processing task data via IPC...");
}

}  // namespace worker
}  // namespace abeille
