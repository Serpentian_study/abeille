add_library(abstract_task INTERFACE)
target_include_directories(abstract_task INTERFACE ${PROJECT_SOURCE_DIR})

add_executable(worker_client
  cmd/main.cpp
  src/dlfcn_wrapper.cpp
  src/worker_client.cpp
  src/registry.cpp
  src/fd.cpp
  src/ipc.cpp
  proto/ipc.pb.cc
)
target_include_directories(worker_client PUBLIC ${PROJECT_SOURCE_DIR})
target_link_libraries(worker_client PUBLIC rpc utils abstract_task)
