#include "raft/core/include/config.hpp"

#include <google/protobuf/util/json_util.h>

#include <algorithm>

#include "common/utils/include/convert.hpp"

using namespace google::protobuf;
using namespace google::protobuf::util;

namespace abeille {
namespace raft {

bool Config::IsOk() const noexcept {
  bool is_ok = true;
  if (user_address_.empty()) {
    LOG_ERROR("User address wasn't set");
    is_ok = false;
  }

  if (raft_address_.empty()) {
    LOG_ERROR("Raft address wasn't set");
    is_ok = false;
  }

  if (worker_address_.empty()) {
    LOG_ERROR("Worker address wasn't set");
    is_ok = false;
  }

  if (snapshot_to.empty()) {
    LOG_ERROR("Snapshot file wasn't set");
    is_ok = false;
  }

  return is_ok;
}

void Config::protoInit(const RaftConfig &proto) noexcept {
  proto_config_ = proto;

  raft_address_ = proto.raft_address();
  user_address_ = proto.user_address();
  worker_address_ = proto.worker_address();
  id_ = address2uint(raft_address_);

  LOG_DEBUG("%s\n%s\n%s", raft_address_.c_str(),
            user_address_.c_str(), worker_address_.c_str());

  peers_ = std::vector<std::string>(proto.peers().begin(), proto.peers().end());
  std::unique(peers_.begin(), peers_.end());
  // implementation of std::erase from c++20
  auto it = std::remove(peers_.begin(), peers_.end(), raft_address_);
  peers_.erase(it, peers_.end());

  /*
  std::cout << "Launching node with peers: ";
  for (auto x : peers_) {
    std::cout << x << ' ';
  }
  std::cout << std::endl;
  */

  // if snapshot name, assuming snapshot was set with cma
  if (!snapshot_to.empty()) {
    proto_config_.set_snapshot_to(snapshot_to);
    proto_config_.set_snapshot_after(snapshot_after_);
  } else {
    snapshot_after_ = proto.snapshot_after();
    snapshot_to = proto.snapshot_to();
  }
}

void Config::Init(std::ifstream &file) {
  util::JsonParseOptions opt;
  opt.ignore_unknown_fields = true;
  opt.case_insensitive_enum_parsing = false;

  std::string file_data = std::string(std::istream_iterator<char>(file),
                                      std::istream_iterator<char>());

  RaftConfig proto_config;
  proto_config_ = proto_config;

  auto status = JsonStringToMessage(file_data, &proto_config, opt);
  if (!status.ok()) {
    LOG_ERROR(status.message().ToString().c_str());
    return;
  }

  protoInit(proto_config);
}

const uint64_t &Config::GetId() const noexcept {
  return id_;
}

const std::string &Config::GetWorkerAddress() const noexcept {
  return worker_address_;
}

const std::string &Config::GetConfigName() const noexcept {
  return config_file_name_;
}

const std::string &Config::GetSnapshotTo() const noexcept {
  return snapshot_to;
}

const std::string &Config::GetUserAddress() const noexcept {
  return user_address_;
}

const std::string &Config::GetRaftAddress() const noexcept {
  return raft_address_;
}

const Config::AddressContainer &Config::GetPeers() const noexcept {
  return peers_;
}

const Index &Config::GetSnapshotAfter() const noexcept {
  return snapshot_after_;
}

void Config::SetConfigName(std::string &str) noexcept {
  config_file_name_ = str;
}

void Config::SetSnapshotAfter(uint64_t after) noexcept {
  snapshot_after_ = after;
}

void Config::SetSnapshotTo(std::string &str) noexcept {
  snapshot_to = str;
}

std::once_flag Config::flag_;
std::shared_ptr<Config> Config::instance_ = nullptr;

}  // namespace raft
}  // namespace abeille
