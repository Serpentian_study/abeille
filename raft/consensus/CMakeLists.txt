add_subdirectory(log)
add_subdirectory(raft_pool)
add_subdirectory(state_machine)
add_subdirectory(raft)
